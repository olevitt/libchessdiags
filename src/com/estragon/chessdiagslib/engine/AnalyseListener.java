package com.estragon.chessdiagslib.engine;

import com.estragon.chessdiagslib.core.Coup;

public interface AnalyseListener {
	public void bestMoveFound(Coup c);
}
