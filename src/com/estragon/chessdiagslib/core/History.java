package com.estragon.chessdiagslib.core;

import java.util.Date;

import android.util.Log;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "history")
public class History {

	@DatabaseField(index = true,id=true) 
	public int problemInternalId;
	
	@DatabaseField
	public Date date;
	
	private Problem problemCache = null;
	
	public History() {
		
	}

	public int getProblemInternalId() {
		return problemInternalId;
	}

	public void setProblemInternalId(int problemInternalId) {
		if (problemInternalId != this.problemInternalId)
			problemCache = null;
		this.problemInternalId = problemInternalId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public String toString() {
		return getProblem().getNom();
	}
	
	public Problem getProblem() {
		if (problemCache != null)
			return problemCache;
		try {
			//return problemCache = DAO.getProblemDao().queryForId(problemInternalId);
		}
		catch (Exception e) {
			Log.e("Chessdiags", "Erreur", e);
		}
		return null;
	}
	
}
