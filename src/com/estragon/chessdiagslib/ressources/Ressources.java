package com.estragon.chessdiagslib.ressources;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.util.Log;

import com.estragon.chessdiagslib.R;

public class Ressources {

	private static final int[] idPieces = new int[] {R.drawable.p0,R.drawable.p1,R.drawable.p2,R.drawable.p3,R.drawable.p4,R.drawable.p5,R.drawable.p6
		,R.drawable.p7,R.drawable.p8,R.drawable.p9,R.drawable.p10,R.drawable.p11,R.drawable.p12};
	public static final Bitmap[] pieces = new Bitmap[13];
	private static Bitmap echiquier = null;
	private static Bitmap echiquier2 = null;

	public synchronized static void charger(final Context context) {
		long millis = System.currentTimeMillis();
		for (int i = 0; i < pieces.length; i++) {
			if (pieces[i] == null) {
				pieces[i] = BitmapFactory.decodeResource(context.getResources(), idPieces[i]);
			}
		}
		if (echiquier == null) {
			echiquier = BitmapFactory.decodeResource(context.getResources(), R.drawable.echiquier);
		}
		if (echiquier2 == null) {
			echiquier2 = BitmapFactory.decodeResource(context.getResources(), R.drawable.echiquier2);
		}
		Log.i("Chessdiags","Ressources loaded in "+(System.currentTimeMillis()-millis)+" ms");
	}

	public synchronized static void waitForLoad() {

	}

	public static Bitmap getEchiquier(Context context) {
		String echiquierPrefs = PreferenceManager.getDefaultSharedPreferences(context).getString("board", ""+R.drawable.echiquier);
		if (echiquierPrefs.equals("0")) {
			if (echiquier == null) {
				echiquier = BitmapFactory.decodeResource(context.getResources(), R.drawable.echiquier);
			}
			return echiquier;
		}

		else {
			if (echiquier2 == null) {
				echiquier2 = BitmapFactory.decodeResource(context.getResources(), R.drawable.echiquier2);
			}
			return echiquier2;

		}
	}
}
